package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.model.User;

public interface IAuthService {

    void checkRoles(@NotNull Role[] roles);

    @NotNull
    User registry(@Nullable String login,
                  @Nullable String password,
                  @Nullable String email);

    void login(@Nullable String login,
               @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

}
